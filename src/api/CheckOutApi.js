export async function checkOutItems(detailsOfCheckOut, chosenPaymentMethod) {
    console.log(detailsOfCheckOut);
    try {
        const token = localStorage.getItem("TestToken");
        const response = await fetch("https://backendcabaccangagain.onrender.com/checkout/checkoutItems", {
            method: "POST",
            body: JSON.stringify({
                items: detailsOfCheckOut.toCheckOutItems,
                totalAmountToPay: detailsOfCheckOut.totalAmountToPay,
                totalItemsForCheckOut: detailsOfCheckOut.totalItemsForCheckOut,
                paymentMethod: chosenPaymentMethod,
            }),
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + token,
                "Content-Type": "application/json",
            },
        });

        const checkedOutItems = await response.json();
        return checkedOutItems;
    } catch (error) {
        return console.log(error);
    }
}
