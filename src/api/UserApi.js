export async function userLogin(userCredentails) {
    try {
        const response = await fetch("https://backendcabaccangagain.onrender.com/users/userLogin", {
            method: "POST",
            body: JSON.stringify({
                email: userCredentails.email,
                password: userCredentails.password,
            }),
            headers: {
                "Content-Type": "application/json",
            },
        });
        const userToken = await response.json();
        if (!userToken.token) return userToken.message;
        return userToken;
    } catch (error) {
        console.log(error.message);
    }
}

export async function userRegistration(userDetails) {
    try {
        const response = await fetch("https://backendcabaccangagain.onrender.com/users/createUser", {
            method: "POST",
            body: JSON.stringify(userDetails),
            headers: {
                "Content-Type": "application/json",
            },
        });
        const newUser = await response.json();
        return newUser;
    } catch (error) {
        console.log(error.message);
    }
}

export async function getUserProfile() {
    try {
        const token = localStorage.getItem("TestToken");
        const response = await fetch(`https://backendcabaccangagain.onrender.com/users/getUserDetails`, {
            headers: {
                Authorization: "Bearer " + token,
                "Content-Type": "application/json",
            },
        });
        const userData = await response.json();
        if (userData) {
            return userData;
        }
    } catch (error) {}
}

export async function updateUser(changesToUser) {
    try {
        const token = localStorage.getItem("TestToken");
        const response = await fetch(
            `https://backendcabaccangagain.onrender.com/users/updateUser/${changesToUser.userId}`,
            {
                method: "PATCH",
                body: JSON.stringify(changesToUser),
                headers: {
                    Authorization: "Bearer " + token,
                    "Content-Type": "application/json",
                },
            }
        );
        const status = await response.json();
        return status;
    } catch (error) {
        console.log(error.message);
    }
}
