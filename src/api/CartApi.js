export async function AddToOrRemoveFromCart(actionType, productDetails, quantity) {
    try {
        const token = localStorage.getItem("TestToken");
        const response = await fetch(`https://backendcabaccangagain.onrender.com/products/${actionType}`, {
            method: "PATCH",
            body: JSON.stringify({
                product: productDetails,
                quantity: quantity,
            }),
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + token,
                "Content-Type": "application/json",
            },
        });
        const updatedUserCart = await response.json();
        return updatedUserCart;
    } catch (error) {
        console.log(error.message);
    }
}

export async function getUserCart() {
    try {
        const token = localStorage.getItem("TestToken");
        const response = await fetch("https://backendcabaccangagain.onrender.com/users/userCart", {
            method: "GET",
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + token,
                "Content-Type": "application/json",
            },
        });

        const userCart = await response.json();
        return userCart;
    } catch (error) {
        console.log(error.message);
    }
}
