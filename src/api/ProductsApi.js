export async function getAvialableProducts(productType, skip) {
    try {
        const response = await fetch(
            `https://backendcabaccangagain.onrender.com/products/${productType}/availableProducts?skip=${skip}`,
            {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
            }
        );
        return await response.json();
    } catch (error) {
        return error;
    }
}

// export async function getFilteredProducts(filter, skip) {
//     try {
//         const response = await fetch(`https://backendcabaccangagain.onrender.com/products/${filter}?skip=${skip}`, {
//             method: "GET",
//             headers: {
//                 Accept: "application/json",
//                 "Content-Type": "application/json",
//             },
//         });
//         return await response.json();
//     } catch (error) {
//         return error;
//     }
// }

export async function getFilteredProducts(filter, value) {
    try {
        const response = await fetch(`https://backendcabaccangagain.onrender.com/products/${filter}/${value}`, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
        });
        const filterdProducts = await response.json();
        return filterdProducts;
    } catch (error) {
        return error;
    }
}

export async function getAllProducts(skip) {
    try {
        const token = localStorage.getItem("TestToken");
        const response = await fetch(`https://backendcabaccangagain.onrender.com/products/allProducts?skip=${skip}`, {
            method: "GET",
            headers: {
                Authorization: "Bearer " + token,
                Accept: "application/json",
                "Content-Type": "application/json",
            },
        });
        return await response.json();
    } catch (error) {
        return error;
    }
}

export async function postProduct(productDetails) {
    try {
        const token = localStorage.getItem("TestToken");
        const response = await fetch("https://backendcabaccangagain.onrender.com/products/createProduct", {
            method: "POST",
            body: JSON.stringify(productDetails),
            headers: {
                Authorization: "Bearer " + token,
                Accept: "application/json",
                "Content-Type": "application/json",
            },
        });

        const productPosted = await response.json();
        console.log(productPosted);
        return productPosted;
    } catch (error) {
        console.log({ message: error.message });
    }
}

export async function updateProduct(productUpdates, productId) {
    try {
        console.log(productId);
        const token = localStorage.getItem("TestToken");
        const response = await fetch(`https://backendcabaccangagain.onrender.com/products/updateProduct/${productId}`, {
            method: "PATCH",
            body: JSON.stringify(productUpdates),
            headers: {
                Authorization: "Bearer " + token,
                Accept: "application/json",
                "Content-Type": "application/json",
            },
        });

        const updatedProduct = await response.json();
        return updatedProduct;
    } catch (error) {
        console.log({ message: error.message });
    }
}

export async function getSingleProduct(productId) {
    try {
        const token = localStorage.getItem("TestToken");
        const response = await fetch(`https://backendcabaccangagain.onrender.com/products/findProduct/${productId}`, {
            headers: {
                Authorization: "Bearer " + token,
                Accept: "application/json",
                "Content-Type": "application/json",
            },
        });
        const productDetails = await response.json();
        return productDetails;
    } catch (error) {
        console.log(error);
    }
}

export async function archiveOrUnarchiveProduct(productId) {
    try {
        const token = localStorage.getItem("TestToken");
        const response = await fetch(`https://backendcabaccangagain.onrender.com/products/archiveProduct/${productId}`, {
            method: "PATCH",
            headers: {
                Authorization: "Bearer " + token,
                Accept: "application/json",
                "Content-Type": "application/json",
            },
        });
        const confirmation = await response.json();
        return confirmation;
    } catch (error) {
        console.log(error);
    }
}
