export async function retrieveOrders() {
    try {
        const token = localStorage.getItem("TestToken");
        const response = await fetch(`https://backendcabaccangagain.onrender.com/orders/history`, {
            method: "GET",
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + token,
                "Content-Type": "application/json",
            },
        });
        const orders = await response.json();
        return orders;
    } catch (error) {
        console.log(error);
    }
}

export async function getFilteredOrder(filterDetails) {
    try {
        const token = localStorage.getItem("TestToken");
        const response = await fetch(`https://backendcabaccangagain.onrender.com/orders/history/filtered`, {
            method: "POST",
            body: JSON.stringify(filterDetails),
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + token,
                "Content-Type": "application/json",
            },
        });
        const orders = await response.json();
        return orders;
    } catch (error) {
        console.log(error);
    }
}

export async function orderRecieved(orderId) {
    try {
        const token = localStorage.getItem("TestToken");
        const response = await fetch(`https://backendcabaccangagain.onrender.com/orders/received/${orderId}`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + token,
                "Content-Type": "application/json",
            },
        });
        const status = await response.json();
        return status;
    } catch (error) {
        console.log(error);
    }
}
