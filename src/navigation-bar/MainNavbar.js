import { Fragment, useContext, useEffect, useState } from "react";
import { Link, useNavigate, NavLink } from "react-router-dom";
import { CartContext } from "../components/store/CartContext";
import { UserContext } from "../components/store/UserContext";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";

export default function MainNavbar() {
    const userContext = useContext(UserContext);
    const cartContext = useContext(CartContext);
    const navigate = useNavigate();

    const logoutHandler = () => {
        localStorage.removeItem("TestToken");
        localStorage.removeItem("isAdmin");
        userContext.setUserToken(null);
        userContext.setUserAdmin(false);
        navigate("/home", { replace: true });
    };

    return (
        <Navbar bg="light" expand="lg" className="vw-100 sticky-top">
            <Container fluid>
                <Navbar.Brand as={NavLink} to="/shop">
                    Tabi-Tabi Lang
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        {
                            userContext.isAdmin && (
                                <Nav.Link as={NavLink} to="/adminPage">
                                    Admin Page
                                </Nav.Link>
                            )
                            // () : (
                            //     <Nav.Link as={NavLink} to="/home">
                            //         Home
                            //     </Nav.Link>
                            // )
                        }
                        {userContext.isAdmin && (
                            <Fragment>
                                <Nav.Link as={NavLink} to="/postProduct">
                                    Post Product
                                </Nav.Link>
                                <Nav.Link as={NavLink} to="/userList">
                                    User List
                                </Nav.Link>
                            </Fragment>
                        )}
                        {!userContext.isAdmin && (
                            <Nav.Link as={NavLink} to="/shop">
                                Shop
                            </Nav.Link>
                        )}
                        {/* {!userContext.isAdmin && (
                            <Nav.Link as={NavLink} to="/shop/discounted">
                                Discounted Items
                            </Nav.Link>
                        )}
                        {!userContext.isAdmin && (
                            <Nav.Link as={NavLink} to="/shop/popular">
                                Popular Items
                            </Nav.Link>
                        )} */}
                        {userContext.token && !userContext.isAdmin && (
                            <Nav.Link as={NavLink} to="/orders">
                                Orders
                            </Nav.Link>
                        )}
                        {userContext.token ? (
                            <Fragment>
                                {!userContext.isAdmin && (
                                    <Nav.Link as={NavLink} to="/userCart">
                                        Cart: {cartContext.totalItems}
                                    </Nav.Link>
                                )}
                                <Nav.Link
                                    as={NavLink}
                                    to="/login"
                                    onClick={() => {
                                        logoutHandler();
                                    }}
                                >
                                    Logout
                                </Nav.Link>
                            </Fragment>
                        ) : (
                            <Fragment>
                                <Nav.Link as={NavLink} to="/login">
                                    Login
                                </Nav.Link>
                                <Nav.Link as={NavLink} to="/registration">
                                    Register
                                </Nav.Link>
                            </Fragment>
                        )}
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}
