import { useContext } from "react";
import { Navigate, Outlet } from "react-router-dom";
import { UserContext } from "../components/store/UserContext";

export default function LoggedInRoutes() {
    const userContext = useContext(UserContext);
    return userContext.token && !userContext.isAdmin ? <Outlet /> : <Navigate to="/login" />;
}
