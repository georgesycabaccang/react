import { Fragment, useEffect, useState } from "react";
import ProductList from "./products/ProductList";
import { getAllProducts, getAvialableProducts, getFilteredProducts } from "../api/ProductsApi";
import "./ShowProducts.css";
import { useParams } from "react-router-dom";
import { Card, Container } from "react-bootstrap";
import SearchBar from "./SearchBar";

const SINGLE_PRODUCT = "singleProduct";
const BUNDLE_PRODUCT = "bundleProduct";

export default function DashBoard() {
    const isAdmin = localStorage.getItem("isAdmin");
    const [isLoading, setIsLoading] = useState(false);
    const [isEnd, setIsEnd] = useState(false);
    const [skipNumber, setSkipNumber] = useState(0);
    const [loadedProducts, setLoadedProducts] = useState([]);
    const [productsFromSearch, setProductsFromSearch] = useState([]);
    const [filterBy, setFilterBy] = useState(null);
    const { filter } = useParams();
    let productType = SINGLE_PRODUCT;

    // useEffect(() => {
    //     hehe();
    // }, [filter]);

    // const hehe = () => {
    //     setFilterBy(filter);
    // };

    // console.log(filterBy);

    useEffect(() => {
        getProducts();
    }, [skipNumber]);

    const getProducts = async () => {
        try {
            let products;
            setIsLoading(true);
            if (isAdmin) {
                products = await getAllProducts(skipNumber);
            } else {
                products = await getAvialableProducts(SINGLE_PRODUCT, skipNumber);
            }

            // let products;
            //
            //     if (filter === "discounted") {
            //         console.log("?");
            //         products = await getFilteredProducts(filter, skipNumber);
            //         setFilterBy(filter);
            //     } else if (filter === "popular") {
            //         products = await getFilteredProducts(filter, skipNumber);
            //         setFilterBy(filter);
            //     } else {

            //     }

            // }

            setLoadedProducts([...loadedProducts, ...products]);
        } catch (error) {
            if (error.message === "products is not iterable") return setIsEnd(true);
            console.log({ message: error.message });
        } finally {
            setIsLoading(false);
        }
    };

    const scrollHandler = (event) => {
        const { scrollHeight, offsetHeight, scrollTop } = event.target;
        const endOfScroll = offsetHeight + scrollTop >= scrollHeight;
        if (endOfScroll) {
            setSkipNumber(loadedProducts.length);
        }
    };

    return (
        <Fragment>
            {/* <SearchBar from={"fromShop"} setProductsFromSearch={setProductsFromSearch} /> */}
            <div className="hideScroll" onScroll={scrollHandler}>
                <ProductList
                    products={productsFromSearch.length != 0 ? productsFromSearch : loadedProducts}
                    productType={productType}
                />
                {isLoading && <Card.Text>Loading...</Card.Text>}
                {isEnd && <Card.Text>You have reached the end</Card.Text>}
            </div>
        </Fragment>
    );
}
