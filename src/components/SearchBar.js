import { Fragment, useContext, useEffect, useState } from "react";
import { Button, Card, Dropdown, Form, InputGroup } from "react-bootstrap";
import { getFilteredOrder } from "../api/OrdersApi";
import { getFilteredProducts } from "../api/ProductsApi";
import "./SearchBar.css";
import { OrderContext } from "./store/OrderContext";

export default function SearchBar(props) {
    const orderContext = useContext(OrderContext);
    const [filter, setFilter] = useState("");
    const [searchValue, setSearchValue] = useState("");
    const [searchValueBar, setSearchValueBar] = useState("");
    const [filterSource, setFilterSource] = useState("");

    const searchHandler = async (event) => {
        event.preventDefault();

        const source = filterSource === "searchBar";

        const searchOrdersBy = {
            filter: filter,
            value: source ? searchValueBar : searchValue,
        };
        orderContext.onSearch(searchOrdersBy);
    };

    const productSearchBarVersion = async () => {
        const filteredProducts = await getFilteredProducts("productName", searchValueBar);
        props.setProductsFromSearch(filteredProducts);
    };

    return (
        <Fragment>
            <Card.Title> {props.from !== "fromShop" ? "Search Orders" : "Search Shop"}</Card.Title>
            <Form onSubmit={searchHandler}>
                <InputGroup className="mb-3 w-50">
                    <Form.Control
                        value={searchValueBar}
                        onChange={(event) => {
                            setFilterSource("searchBar");
                            setFilter("productOrdered");
                            setSearchValueBar(event.target.value);
                        }}
                    />
                    {props.from !== "fromShop" ? (
                        <Button
                            type="submit"
                            onClick={() => {
                                setFilterSource("searchBar");
                                setFilter("productOrdered");
                            }}
                            variant="outline-secondary"
                            id="button-addon1"
                        >
                            Search
                        </Button>
                    ) : (
                        <Button
                            type="submit"
                            onClick={() => {
                                productSearchBarVersion();
                            }}
                            variant="outline-secondary"
                            id="button-addon1"
                        >
                            Search
                        </Button>
                    )}
                </InputGroup>
                {props.from !== "fromShop" && (
                    <Fragment>
                        <Button
                            onClick={() => {
                                orderContext.onGetAll();
                            }}
                            variant="outline-secondary"
                            id="allOrdersBtn"
                        >
                            All Orders
                        </Button>
                        <Button
                            type="submit"
                            onClick={() => {
                                setFilterSource("button");
                                setFilter("isPaid");
                                setSearchValue(false);
                            }}
                            variant="outline-secondary"
                            id="allOrdersBtn"
                        >
                            To Pay Orders
                        </Button>
                        <Button
                            type="submit"
                            onClick={() => {
                                setFilterSource("button");
                                setFilter("isPaid");
                                setSearchValue(true);
                            }}
                            variant="outline-secondary"
                            id="allOrdersBtn"
                        >
                            Paid Orders
                        </Button>
                        <Button
                            type="submit"
                            onClick={() => {
                                setFilterSource("button");
                                setFilter("status");
                                setSearchValue("Order Pending");
                            }}
                            variant="outline-secondary"
                            id="allOrdersBtn"
                        >
                            Pending Orders
                        </Button>
                        <Button
                            type="submit"
                            onClick={() => {
                                setFilterSource("button");
                                setFilter("status");
                                setSearchValue("Received");
                            }}
                            variant="outline-secondary"
                            id="allOrdersBtn"
                        >
                            Received
                        </Button>{" "}
                    </Fragment>
                )}
            </Form>
        </Fragment>
    );
}

{
    /* <Dropdown className="dropDownInSearchBar">
    <Dropdown.Toggle variant="success" id="dropdown-basic" className="dropDownInSearchBar">
        {searchFilter}
    </Dropdown.Toggle>

    <Dropdown.Menu>
        <Dropdown.Item
            onClick={() => {
                setFilter("status");
            }}
        >
            Status
        </Dropdown.Item>
        <Dropdown.Item
            onClick={() => {
                setFilter("isPaid");
            }}
        >
            Payment Status
        </Dropdown.Item>
        <Dropdown.Item
            onClick={() => {
                setFilter("productOrdered");
            }}
        >
            Product Name
        </Dropdown.Item>
    </Dropdown.Menu>
</Dropdown>; */
}
