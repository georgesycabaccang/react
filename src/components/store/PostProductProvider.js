import { useReducer } from "react";
import { PostProductContext } from "./PostProductContext";

const ADD_VARIATION = "addVariation";
const REMOVE_VARIATION = "removeVariation";
const ADD_IMAGES = "addImages";

const defaultVariations = {
    productVariations: [],
    productImagesPreview: [],
};

const variationReducer = (state, action) => {
    if (action.actionType === ADD_VARIATION) {
        const key = Object.keys(action.variation);
        const value = Object.values(action.variation);
        const variationType = key[0];
        const variations = value[0];
        const newVaraition = {
            id: variationType,
            [variationType]: variations,
        };
        const updatedProductVariations = state.productVariations.concat(newVaraition);

        return {
            productVariations: updatedProductVariations,
            productImagesPreview: state.productImagesPreview,
        };
    }

    if (action.actionType === REMOVE_VARIATION) {
        const key = Object.keys(action.id);
        const updatedProductVariations = state.productVariations.filter(
            (variation) => variation.id !== action.id
        );

        return {
            productVariations: updatedProductVariations,
            productImagesPreview: state.productImagesPreview,
        };
    }

    if (action.actionType === ADD_IMAGES) {
        return {
            productVariations: state.productVariations,
            productImagesPreview: action.images,
        };
    }
};

export default function PostProductProvider(props) {
    const [varationsState, dispatchAction] = useReducer(variationReducer, defaultVariations);

    const onAddVariation = (variation) => {
        dispatchAction({ actionType: ADD_VARIATION, variation: variation });
    };

    const onRemoveVariation = (variationId) => {
        dispatchAction({ actionType: REMOVE_VARIATION, id: variationId });
    };
    const onAddImages = (images) => {
        dispatchAction({ actionType: ADD_IMAGES, images: images });
    };

    const postProductContext = {
        productVariations: varationsState.productVariations,
        productImagesPreview: varationsState.productImagesPreview,
        addImages: onAddImages,
        addVariation: onAddVariation,
        removeVariation: onRemoveVariation,
    };

    return (
        <PostProductContext.Provider value={postProductContext}>
            {props.children}
        </PostProductContext.Provider>
    );
}
