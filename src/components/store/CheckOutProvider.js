import { useEffect, useReducer } from "react";
import { useLocation } from "react-router-dom";
import { CheckOutContext } from "./CheckOutContext";

const ADD_TO_CHECKOUT = "add";
const REMOVE_FROM_CHECKOUT = "remove";
const CHANGED_LOCATION = "changedLocation";

const defaultToBeCheckedOut = {
    toCheckOutItems: [],
    totalItemsForCheckout: 0,
    totalAmountToPay: 0,
};

const checkOutReducer = (state, action) => {
    if (action.actionType === CHANGED_LOCATION) {
        return {
            toCheckOutItems: [],
            totalItemsForCheckout: 0,
            totalAmountToPay: 0,
        };
    }

    if (action.actionType === ADD_TO_CHECKOUT) {
        const updatedItems = state.toCheckOutItems.concat(action.item);
        const updatedItemsForCheckout = state.totalItemsForCheckout + 1;
        const updatedAmountToPay = Math.abs(
            state.totalAmountToPay + action.item.totalPrice * action.item.quantity
        );

        return {
            toCheckOutItems: updatedItems,
            totalItemsForCheckout: updatedItemsForCheckout,
            totalAmountToPay: updatedAmountToPay,
        };
    }

    if (action.actionType === REMOVE_FROM_CHECKOUT) {
        const updatedItems = state.toCheckOutItems.filter(
            (checkedItem) => checkedItem._id !== action.item._id
        );
        const updatedItemsForCheckout = state.totalItemsForCheckout - 1;
        const updatedAmountToPay = Math.abs(
            state.totalAmountToPay - action.item.totalPrice * action.item.quantity
        );
        return {
            toCheckOutItems: updatedItems,
            totalItemsForCheckout: updatedItemsForCheckout,
            totalAmountToPay: updatedAmountToPay,
        };
    }
};

export default function CheckOutProvider(props) {
    const location = useLocation();
    const [checkOutState, dispatchAction] = useReducer(checkOutReducer, defaultToBeCheckedOut);

    useEffect(() => {
        onChangeLocation();
    }, [location.pathname]);

    const onChangeLocation = () => {
        dispatchAction({ actionType: CHANGED_LOCATION, item: null });
    };

    const addToCheckOut = (itemDetails) => {
        dispatchAction({ actionType: ADD_TO_CHECKOUT, item: itemDetails });
    };
    const removeFromCheckOut = (itemDetails) => {
        dispatchAction({ actionType: REMOVE_FROM_CHECKOUT, item: itemDetails });
    };

    const checkOutContext = {
        toCheckOutItems: checkOutState.toCheckOutItems,
        totalItemsForCheckout: checkOutState.totalItemsForCheckout,
        totalAmountToPay: checkOutState.totalAmountToPay,
        onCheck: addToCheckOut,
        onUncheck: removeFromCheckOut,
    };

    return (
        <CheckOutContext.Provider value={checkOutContext}>
            {props.children}
        </CheckOutContext.Provider>
    );
}
