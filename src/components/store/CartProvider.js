import { useEffect, useReducer } from "react";
import { AddToOrRemoveFromCart, getUserCart } from "../../api/CartApi";
import { CartContext } from "./CartContext";

const ADD = "addToCart";
const REMOVE = "removeFromCart";
const LOAD_CART = "loadCart";

const defaultCart = {
    items: [],
    totalItems: 0,
    totalAmount: 0,
};

const cartReducer = (state, action) => {
    if (action.actionType === LOAD_CART) {
        const initialItems = action.items;
        const initialTotalItems = action.totalItems;
        const initialTotalAmount = action.totalAmount;

        return {
            items: initialItems,
            totalItems: initialTotalItems,
            totalAmount: initialTotalAmount,
        };
    }
    //
    //
    // ADD TO CART
    //
    //
    if (action.actionType === ADD) {
        const addToCart = async () => {
            if (action.item.productDetails === undefined) {
                let productDetails = action.item;
                const userCart = await AddToOrRemoveFromCart(
                    ADD,
                    productDetails,
                    productDetails.quantity
                );
                return userCart;
            } else {
                const userCart = await AddToOrRemoveFromCart(
                    ADD,
                    action.item.productDetails,
                    action.item.quantity
                );
                return userCart;
            }
        };
        addToCart();

        const existingItemIndex = state.items.findIndex(
            (item) => item.productId === action.item.productId
        );
        const itemExistsIsCart = existingItemIndex !== -1;

        let updatedCartItems;
        if (itemExistsIsCart) {
            const existingItem = state.items[existingItemIndex];

            const updatedItem = {
                ...existingItem,
                quantity: existingItem.quantity + action.item.quantity,
            };
            updatedCartItems = [...state.items];
            updatedCartItems[existingItemIndex] = updatedItem;
        } else {
            updatedCartItems = state.items.concat(action.item); // used to be push
        }

        const numberOfCartItems = updatedCartItems.reduce((current, item) => {
            return current + item.quantity;
        }, 0);

        const updatedTotalAmount =
            state.totalAmount + action.item.totalPrice * action.item.quantity;

        return {
            items: updatedCartItems,
            totalItems: numberOfCartItems,
            totalAmount: updatedTotalAmount,
        };
    }
    //
    //
    // REMOVE FROM CART
    //
    //
    if (action.actionType === REMOVE) {
        const removeFromCart = async () => {
            await AddToOrRemoveFromCart(REMOVE, action.item);
            // console.log(userCart);
        };
        removeFromCart();
        const existingItemIndex = state.items.findIndex(
            (item) => item.productId === action.item.productId
        );
        const itemExistsIsCart = existingItemIndex !== -1;

        let updatedCartItems;
        let updatedTotalAmount;
        if (itemExistsIsCart) {
            const existingItem = state.items[existingItemIndex];

            if (existingItem.quantity === 1) {
                updatedCartItems = state.items.filter(
                    (item) => item.productId !== action.item.productId
                );
            } else {
                const updatedItem = {
                    ...existingItem,
                    quantity: existingItem.quantity - 1,
                };
                updatedCartItems = [...state.items];
                updatedCartItems[existingItemIndex] = updatedItem;
            }
            updatedTotalAmount = state.totalAmount - existingItem.totalPrice;
        }

        const numberOfCartItems = updatedCartItems.reduce((current, item) => {
            return current + item.quantity;
        }, 0);

        return {
            items: updatedCartItems,
            totalItems: numberOfCartItems,
            totalAmount: updatedTotalAmount,
        };
    }

    return defaultCart;
};

export default function CartProvider(props) {
    const [cartState, dispatchAction] = useReducer(cartReducer, defaultCart);

    useEffect(() => {
        getCartItems();
    }, []);

    const getCartItems = async () => {
        const userCart = await getUserCart();
        dispatchAction({
            actionType: LOAD_CART,
            items: userCart.items,
            totalItems: userCart.totalItems,
        });
    };

    const addToCart = (productDetails) => {
        dispatchAction({ actionType: ADD, item: productDetails });
    };
    const removeFromCart = (productDetails) => {
        dispatchAction({ actionType: REMOVE, item: productDetails });
    };

    const cartContext = {
        items: cartState.items,
        totalItems: cartState.totalItems,
        totalAmount: cartState.totalAmount,
        addProduct: addToCart,
        removeProduct: removeFromCart,
        loadUserCart: getCartItems,
    };

    return <CartContext.Provider value={cartContext}>{props.children}</CartContext.Provider>;
}
// IF PRODUCT NOT FOUND, REMOVE FROM CART
