import { createContext } from "react";

export const CheckOutContext = createContext({
    toCheckOutItems: [],
    totalItemsForCheckout: 0,
    totalAmountToPay: 0,
    onCheck: (item) => {},
    onUncheck: (item) => {},
    onCheckOut: () => {},
});
