import { createContext } from "react";

export const OrderContext = createContext({
    orders: [],
    onSearch: (filterDetails) => {},
    onGetAll: () => {},
    resetter: () => {},
});
