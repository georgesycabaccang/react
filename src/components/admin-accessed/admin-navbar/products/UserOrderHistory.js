import { Fragment } from "react";
import { Accordion, Card, ListGroup } from "react-bootstrap";
import GroupedUserHistory from "./GroupedUserHistory";

export default function UserOrderHistory(props) {
    console.log(props);
    return (
        <Fragment>
            <Card>
                <Accordion>
                    <Accordion.Item eventKey={props.user._id}>
                        <Accordion.Header>
                            {props.user.firstName} {props.user.lastName}'s Order History
                        </Accordion.Header>
                        <Accordion.Body>
                            <ListGroup>
                                {props.user.orders.length === 0 && (
                                    <Card.Text>No Orders...</Card.Text>
                                )}
                                {props.user.orders.map((order) => {
                                    return (
                                        <ListGroup.Item>
                                            <GroupedUserHistory order={order} />
                                        </ListGroup.Item>
                                    );
                                })}
                            </ListGroup>
                        </Accordion.Body>
                    </Accordion.Item>
                </Accordion>
            </Card>
        </Fragment>
    );
}
