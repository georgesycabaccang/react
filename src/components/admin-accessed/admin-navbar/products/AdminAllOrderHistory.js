import { Fragment, useEffect, useState } from "react";
import { Button, Card, ListGroup } from "react-bootstrap";
import UserOrderHistory from "./UserOrderHistory";

export default function AdminAllOrderHistory() {
    const [isLoading, setIsLoading] = useState(false);
    const [loadedUsers, setLoadedUsers] = useState([]);
    const [userHasUpdate, setUserHasUpdated] = useState(false);

    useEffect(() => {
        if (userHasUpdate) return setUserHasUpdated(false);

        try {
            const getAllUsers = async () => {
                const token = localStorage.getItem("TestToken");

                setIsLoading(true);
                const response = await fetch("https://backendcabaccangagain.onrender.com/users", {
                    method: "GET",
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json",
                    },
                });
                const users = await response.json();
                setLoadedUsers(users);
                setIsLoading(false);
            };
            getAllUsers();
        } catch (error) {
            console.log(error);
        }
    }, [setLoadedUsers, setIsLoading, userHasUpdate]);

    return (
        <Fragment>
            <Card className="d-flex align-items-center w-100">
                <Card.Title>Order History Of Users</Card.Title>
                <ListGroup className="w-75">
                    {loadedUsers.map((user) => {
                        return (
                            <ListGroup.Item>
                                <UserOrderHistory user={user} />
                            </ListGroup.Item>
                        );
                    })}
                </ListGroup>
            </Card>
        </Fragment>
    );
}
