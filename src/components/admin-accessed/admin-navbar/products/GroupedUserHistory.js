import { Fragment } from "react";
import { Accordion, Card } from "react-bootstrap";

export default function GroupedUserHistory(props) {
    console.log(props);
    const date = new Date(props.order.createdAt).toString().split("G")[0];
    let piece = "piece";
    if (props.order.order.quantity > 1) {
        piece = "pieces";
    }

    return (
        <Fragment>
            <Card>
                <Card.Text>Purchased On: {date}</Card.Text>
                <Card.Text>
                    {props.order.productOrdered} - x{props.order.order.quantity} {piece}
                </Card.Text>
                <Card.Text>Total Amount: ₱{props.order.totalAmoutToBePaid}</Card.Text>
            </Card>
        </Fragment>
    );
}
