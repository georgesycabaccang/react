import { Fragment, useContext, useEffect, useState } from "react";
import { Button, Card, Col, Container, Form, Modal, Row } from "react-bootstrap";
import Swal from "sweetalert2";
import { postProduct } from "../../../../api/ProductsApi";
import VariationList from "../../../products/VariationList";
import { PostProductContext } from "../../../store/PostProductContext";
import "./PostProduct.css";
import Image from "react-bootstrap/Image";
import ImagesPreview from "./ImagesPreview";

export default function PostProduct() {
    const postProductContext = useContext(PostProductContext);
    const [show, setShow] = useState(false);
    const [variationTypeInput, setVariationTypeInput] = useState("");
    const [variationInput, setVariationInput] = useState("");
    const [isVarValid, setIsVarValid] = useState(false);
    const [isValid, setIsValid] = useState(false);
    const [images, setImages] = useState([]);

    const [productNameInput, setProductNameInput] = useState("");
    const [descriptionInput, setDescriptionInput] = useState("");
    const [priceInput, setPriceInput] = useState("");
    const [stockInput, setStockInput] = useState("");
    const idsOfImages = [];

    useEffect(() => {}, [images]);

    useEffect(() => {
        if (variationInput && variationTypeInput) return setIsVarValid(true);
        setIsVarValid(false);
    }, [variationInput, variationTypeInput]);

    useEffect(() => {
        if (productNameInput && descriptionInput && priceInput && stockInput)
            return setIsValid(true);
        setIsValid(false);
    }, [productNameInput, descriptionInput, priceInput, stockInput]);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const addVartionHandler = async (event) => {
        event.preventDefault();

        const arrayOfVariations = variationInput.split(", ");
        const variationDetails = {
            [variationTypeInput]: arrayOfVariations,
        };

        postProductContext.addVariation(variationDetails);
        console.log(postProductContext.productVariations);
    };

    console.log(postProductContext.productImagesPreview);

    const postProductHandler = async () => {
        const newProduct = {
            productImages: idsOfImages,
            productName: productNameInput,
            description: descriptionInput,
            originalPrice: priceInput,
            variations: postProductContext.productVariations,
            stock: stockInput,
        };

        const postedProduct = await postProduct(newProduct);
        Swal.fire({
            title: "Product Successfully Created!",
            icon: "success",
            text: "Yududuy!",
        });
    };

    const mulptipleUpload = (event) => {
        event.preventDefault();
        for (let i = 0; i < postProductContext.productImagesPreview.length; i++) {
            const uploadImage = async () => {
                const formData = new FormData();
                formData.append("file", postProductContext.productImagesPreview[i]);
                formData.append("upload_preset", "x4lobvyg");
                const response = await fetch(
                    "https://api.cloudinary.com/v1_1/dpxtfxw52/image/upload",
                    {
                        method: "POST",
                        body: formData,
                    }
                );
                const data = await response.json();
                console.log(data);
                idsOfImages.push(data.public_id);
            };
            uploadImage();
        }
        setTimeout(() => {
            postProductContext.addImages([]);
            postProductHandler();
        }, 2000);
    };

    return (
        <Fragment>
            <Container className="mt-3">
                <Card className="w-100 p-3">
                    {/* {postProductContext.productImagesPreview.legnth != 0 && (
                        <Card>
                            {postProductContext.productImagesPreview.map((image) => {
                                <ImagesPreview image={image} />;
                            })}
                        </Card>
                    )} */}
                    {images.length != 0 && (
                        <Container>
                            <Row>
                                <Col className="col-2">
                                    <Image
                                        fluid
                                        className="imageContainerPostProduct"
                                        src={URL.createObjectURL(images[0])}
                                    />
                                </Col>
                                <Col className="col-2">
                                    {images[1] && (
                                        <Image
                                            fluid
                                            className="imageContainerPostProduct"
                                            src={URL.createObjectURL(images[1])}
                                        />
                                    )}
                                </Col>
                                <Col className="col-2">
                                    {images[2] && (
                                        <Image
                                            fluid
                                            className="imageContainerPostProduct"
                                            src={URL.createObjectURL(images[2])}
                                        />
                                    )}
                                </Col>
                            </Row>
                        </Container>
                    )}
                    <input
                        type="file"
                        multiple
                        onChange={(event) => {
                            setImages(event.target.files);
                            postProductContext.addImages(event.target.files);
                        }}
                    />
                    <Form onSubmit={mulptipleUpload}>
                        <Form.Group className="mb-3" controlId="name">
                            <Form.Label>Name Of Product</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Name"
                                value={productNameInput}
                                onChange={(event) => {
                                    setProductNameInput(event.target.value);
                                }}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="desc">
                            <Form.Label>Product Description</Form.Label>
                            <Form.Control
                                as="textarea"
                                placeholder="Description"
                                value={descriptionInput}
                                onChange={(event) => {
                                    setDescriptionInput(event.target.value);
                                }}
                            />
                        </Form.Group>
                        <Form.Label className="col-12">Vairations</Form.Label>
                        {postProductContext.productVariations.length !== 0 && (
                            <Container fluid>
                                <Row>
                                    <Col>
                                        <VariationList />
                                    </Col>
                                </Row>
                            </Container>
                        )}

                        <Modal show={show} onHide={handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>Add Variation</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Form>
                                    <Form.Group className="mb-3" controlId="var">
                                        <Form.Label>Variation Type:</Form.Label>
                                        <Form.Control
                                            type="text"
                                            onChange={(event) => {
                                                setVariationTypeInput(event.target.value);
                                            }}
                                        />
                                    </Form.Group>

                                    <Form.Group className="mb-3" controlId="var2">
                                        <Form.Label>Variation: </Form.Label>

                                        <Form.Control
                                            type="text"
                                            onChange={(event) => {
                                                setVariationInput(event.target.value);
                                            }}
                                        />
                                        <Form.Text className="text-muted">
                                            Please Enter Variations separated by a comma and a space
                                            &#40;," "&#41;
                                        </Form.Text>
                                    </Form.Group>
                                </Form>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose}>
                                    Close
                                </Button>
                                <Button
                                    type="button"
                                    variant="primary"
                                    disabled={!isVarValid}
                                    onClick={(event) => {
                                        addVartionHandler(event);
                                        handleClose();
                                    }}
                                >
                                    Add Variation
                                </Button>
                            </Modal.Footer>
                        </Modal>

                        <Button className="mb-3" variant="primary" onClick={handleShow}>
                            Add Variation
                        </Button>
                        <Form.Group className="mb-3" controlId="price">
                            <Form.Label>Price Of Product</Form.Label>
                            <Form.Control
                                type="number"
                                placeholder="Price"
                                value={priceInput}
                                onChange={(event) => {
                                    setPriceInput(event.target.value);
                                }}
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="stock">
                            <Form.Label>Stock Available</Form.Label>
                            <Form.Control
                                type="number"
                                value={stockInput}
                                placeholder="Stock"
                                onChange={(event) => {
                                    setStockInput(event.target.value);
                                }}
                            />
                        </Form.Group>

                        <Button variant="primary" type="submit" disabled={!isValid}>
                            Post Product
                        </Button>
                    </Form>
                </Card>
            </Container>
        </Fragment>
    );
}
