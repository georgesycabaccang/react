import { useEffect, useState } from "react";
import { Form, Button, Row, Col, Container } from "react-bootstrap";
import Swal from "sweetalert2";
import { updateUser } from "../../api/UserApi";

export default function UpdateUserForm(props) {
    const [updateSuccess, setUpdateSuccess] = useState(false);
    const user = props.user;
    const [emailInput, seteEmailInput] = useState(user.email);
    const [passwordInput, setPasswordInput] = useState("********");
    const [firstNameInput, setFirstNameInput] = useState(user.firstName);
    const [lastNameInput, setLastNameInput] = useState(user.lastName);
    const [mobileNumInput, setMobileNumInput] = useState(user.mobileNum);
    const [streetNumberAndNameInput, setStreetNumberAndNameInput] = useState(
        user.address[0].streetNumberAndName
    );
    const [brgyDistrictInput, setBrgyDistrictInput] = useState(user.address[0].brgyDistrict);
    const [cityMunicipalityInput, setCityMunicipalityInput] = useState(
        user.address[0].cityMunicipality
    );
    const [countryInput, setCountryInput] = useState(user.address[0].country);
    const [postalCodeInput, setPostalCodeInput] = useState(user.address[0].postalCode);

    useEffect(() => {
        setUpdateSuccess(false);
    }, [updateSuccess]);

    const onUpdateHandler = async (event) => {
        event.preventDefault();

        const updatedUserDetails = {
            userId: user._id,
            email: emailInput,
            firstName: firstNameInput,
            lastName: lastNameInput,
            mobileNum: mobileNumInput,
            address: {
                streetNumberAndName: streetNumberAndNameInput,
                brgyDistrict: brgyDistrictInput,
                cityMunicipality: cityMunicipalityInput,
                country: countryInput,
                postalCode: postalCodeInput,
            },
        };

        const response = await updateUser(updatedUserDetails);
        if (response.message === "Update Successful!") {
            Swal.fire({
                title: response.message,
                icon: "success",
                text: "Please Check User.",
            }).then(() => {
                setUpdateSuccess(true);
            });
        }
    };

    return (
        <Form onSubmit={onUpdateHandler}>
            <Container>
                <Row className="m-3">
                    <Col className="col-6">
                        <Form.Group className="mb-3" controlId={`${user._id}emai`}>
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                value={emailInput}
                                onChange={(event) => {
                                    seteEmailInput(event.target.value);
                                }}
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId={`${user._id}pw`}>
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                readOnly
                                type="password"
                                placeholder="Password"
                                value={passwordInput}
                                onChange={(event) => {
                                    setPasswordInput(event.target.value);
                                }}
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId={`${user._id}fn`}>
                            <Form.Label>First Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="First Name"
                                value={firstNameInput}
                                onChange={(event) => {
                                    setFirstNameInput(event.target.value);
                                }}
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId={`${user._id}ln`}>
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Last Name"
                                value={lastNameInput}
                                onChange={(event) => {
                                    setLastNameInput(event.target.value);
                                }}
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId={`${user._id}num`}>
                            <Form.Label>Mobile Number</Form.Label>
                            <Form.Control
                                type="number"
                                placeholder="Mobile Number"
                                value={mobileNumInput}
                                onChange={(event) => {
                                    setMobileNumInput(event.target.value);
                                }}
                            />
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group className="mb-3" controlId={`${user._id}street`}>
                            <Form.Label>Street</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Street"
                                value={streetNumberAndNameInput}
                                onChange={(event) => {
                                    setStreetNumberAndNameInput(event.target.value);
                                }}
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId={`${user._id}brgy`}>
                            <Form.Label>Baranggay</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Baranggay"
                                value={brgyDistrictInput}
                                onChange={(event) => {
                                    setBrgyDistrictInput(event.target.value);
                                }}
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId={`${user._id}city`}>
                            <Form.Label>City</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="City"
                                value={cityMunicipalityInput}
                                onChange={(event) => {
                                    setCityMunicipalityInput(event.target.value);
                                }}
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId={`${user._id}country`}>
                            <Form.Label>Country</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Country"
                                value={countryInput}
                                onChange={(event) => {
                                    setCountryInput(event.target.value);
                                }}
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId={`${user._id}zip`}>
                            <Form.Label>Zip Code</Form.Label>
                            <Form.Control
                                type="number"
                                placeholder="Zip Code"
                                value={postalCodeInput}
                                onChange={(event) => {
                                    setPostalCodeInput(event.target.value);
                                }}
                            />
                        </Form.Group>
                    </Col>
                </Row>
            </Container>
            <Button type="submit" variant="primary">
                Submit
            </Button>
        </Form>
    );
}
