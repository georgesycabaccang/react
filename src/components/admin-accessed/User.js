import { useState } from "react";
import { Accordion, Button, Card, Col, Container, Row } from "react-bootstrap";

import UpdateUserForm from "./UpdateUserForm";

export default function User(props) {
    const user = props.userDetails;
    const [isAdmin, setIsAdmin] = useState(user.isAdmin);
    console.log(user);

    const changeRoleHandler = async () => {
        try {
            const token = localStorage.getItem("TestToken");
            const response = await fetch(`https://backendcabaccangagain.onrender.com/users/changeRole/${user._id}`, {
                method: "PATCH",
                headers: { Authorization: "Bearer " + token },
            });
            await response.json();
            setIsAdmin(!isAdmin);
        } catch (error) {
            console.log(error.message);
        }
    };

    return (
        <Card>
            <Accordion>
                <Accordion.Item eventKey={user._id}>
                    <Accordion.Header>
                        <Container>
                            <Row>
                                <Col className="col-6">
                                    {user.firstName} {user.lastName}
                                </Col>
                                <Col className="col-6 d-flex justify-content-end">
                                    <Card.Text>Update User</Card.Text>
                                </Col>
                            </Row>
                        </Container>
                    </Accordion.Header>
                    <Accordion.Body>
                        <UpdateUserForm user={user} />
                    </Accordion.Body>
                </Accordion.Item>
            </Accordion>
            <Container
                fluid
                className=" border-0 w-100 d-flex text-align-end align-items-end justify-content-end"
            >
                <Button
                    className={
                        isAdmin ? "w-25 bg-success bg-gradient" : "w-25 bg-danger bg-gradient"
                    }
                    type="button"
                    value="Toggle Admin"
                    onClick={() => {
                        changeRoleHandler();
                    }}
                >
                    Toggle Role ({isAdmin ? "Admin" : "Non-Admin"})
                </Button>
            </Container>
        </Card>
    );
}
