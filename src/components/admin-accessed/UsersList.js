import { Fragment, useEffect, useState } from "react";
import { Card, ListGroup, ListGroupItem } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import User from "./User";

export default function UsersList(props) {
    const [isLoading, setIsLoading] = useState(false);
    const [loadedUsers, setLoadedUsers] = useState([]);
    const [userHasUpdate, setUserHasUpdated] = useState(false);

    useEffect(() => {
        if (userHasUpdate) return setUserHasUpdated(false);

        try {
            const getAllUsers = async () => {
                const token = localStorage.getItem("TestToken");

                setIsLoading(true);
                const response = await fetch("https://backendcabaccangagain.onrender.com/users", {
                    method: "GET",
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json",
                    },
                });
                const users = await response.json();
                setLoadedUsers(users);
                setIsLoading(false);
            };
            getAllUsers();
        } catch (error) {
            console.log(error);
        }
    }, [userHasUpdate]);

    if (isLoading) {
        return (
            <Fragment>
                <Card.Text>User List</Card.Text>
                <Card.Text>Loading User...</Card.Text>
            </Fragment>
        );
    }

    return (
        <Card className="w-100 d-flex align-items-center">
            <Card.Text className="mt-4">Users List</Card.Text>
            <ListGroup className="w-100">
                {loadedUsers.map((user) => {
                    return (
                        <ListGroupItem key={user._id}>
                            <User
                                key={user._id}
                                userDetails={user}
                                setUserHasUpdated={setUserHasUpdated}
                            />
                        </ListGroupItem>
                    );
                })}
            </ListGroup>
        </Card>
    );
}
