import { ListGroup } from "react-bootstrap";
import "./CheckOutItem.css"

export default function CheckOutItem(props) {
    const item = props.itemDetails;

    const totalAmount = item.totalPrice * item.quantity;
    let moreThanOne = "item";
    if (item.quantity > 1) {
        moreThanOne = "items";
    }

    const totalItems = `(${item.quantity} ${moreThanOne})`;

    return (
        <ListGroup.Item className='checkOutItemCard'>
            <h4>{item.productName}</h4>
            <h5>P {item.totalPrice}</h5>
            <h5>x{item.quantity}</h5>
            <h6>Shipping Fee: P {props.defaultShippingFee}</h6>
            <h5>
                Total Amount {totalItems}: P {totalAmount.toFixed(2)}
            </h5>
        </ListGroup.Item>
    );
}
