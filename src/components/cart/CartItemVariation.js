import { Card, Container } from "react-bootstrap";
import "./CartItemVariation.css";

export default function CartItemVariation(props) {
    return (
        <Container fluid className="p-0">
            <Card className="variationCardInCart m-0">
                <Card.Text>{props.variation}</Card.Text>
            </Card>
        </Container>
    );
}
