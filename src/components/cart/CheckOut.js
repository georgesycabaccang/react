import { useContext } from "react";
import { CheckOutContext } from "../store/CheckOutContext";
import CheckOutItem from "./CheckOutItem";
import { Card, ListGroup, Modal, Container, Row, Col, Form } from "react-bootstrap";

export default function ProductList(props) {
    const checkOutContext = useContext(CheckOutContext);
    const defaultShippingFee = 69;
    const shippingFeeSubtotal = defaultShippingFee * checkOutContext.toCheckOutItems.length;
    const totalAmountToPay = checkOutContext.totalAmountToPay + shippingFeeSubtotal;

    return (
        <Form>
            <Modal
                show={props.isCheckingOut}
                onHide={() => props.setIsCheckingOut(false)}
                dialogClassName="modalSize mb-0 p-0"
                aria-labelledby="example-custom-modal-styling-title"
            >
                <Modal.Header closeButton>
                    <Modal.Title id="example-custom-modal-styling-title">Check Out</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <Row>
                            <Col className="col-8">
                                <Card className="leftSidePaneModalCard">
                                    <ListGroup className="userCartUList">
                                        {checkOutContext.toCheckOutItems.map((item) => {
                                            return (
                                                <CheckOutItem
                                                    key={item._id}
                                                    itemDetails={item}
                                                    defaultShippingFee={defaultShippingFee}
                                                />
                                            );
                                        })}
                                    </ListGroup>
                                </Card>
                            </Col>
                            <Col className="col-4 ms-0 px-0">
                                <Card className="rightSidePaneModal p-2">
                                    <Card.Text className="paymentMethodLabel">
                                        Payment Method
                                    </Card.Text>
                                    <Container fluid className="p-0">
                                        <Row className="">
                                            <Col className="col-6 px-0 paymentCol d-flex justify-content-center ">
                                                <button
                                                    type="button"
                                                    className="paymentMethodLabelBtns"
                                                    onClick={() => {
                                                        props.setChosenPaymentMethod(
                                                            "Cash On Delivery"
                                                        );
                                                    }}
                                                >
                                                    Cash On Delivery
                                                </button>
                                            </Col>
                                            <Col className="col-6 px-0 paymentCol d-flex justify-content-center">
                                                <button
                                                    type="button"
                                                    className="paymentMethodLabelBtns"
                                                    onClick={() => {
                                                        props.setChosenPaymentMethod(
                                                            "Linked Bank Account"
                                                        );
                                                    }}
                                                >
                                                    Linked Bank Account
                                                </button>
                                            </Col>
                                            <Col className="col-6 px-0 paymentCol d-flex justify-content-center">
                                                <button
                                                    type="button"
                                                    className="paymentMethodLabelBtns"
                                                    onClick={() => {
                                                        props.setChosenPaymentMethod(
                                                            "Online Banking"
                                                        );
                                                    }}
                                                >
                                                    Online Banking
                                                </button>
                                            </Col>
                                            <Col className="col-6 px-0 paymentCol d-flex justify-content-center">
                                                <button
                                                    type="button"
                                                    className="paymentMethodLabelBtns"
                                                    onClick={() => {
                                                        props.setChosenPaymentMethod("Google Pay");
                                                    }}
                                                >
                                                    Google Pay
                                                </button>
                                            </Col>
                                            <Col className="col-6 px-0 paymentCol d-flex justify-content-center">
                                                <button
                                                    type="button"
                                                    className="paymentMethodLabelBtns"
                                                    onClick={() => {
                                                        props.setChosenPaymentMethod(
                                                            "Payment Center/ e-Wallet"
                                                        );
                                                    }}
                                                >
                                                    Payment Center/ e-Wallet
                                                </button>
                                            </Col>
                                        </Row>
                                    </Container>
                                    <Container fluid className="p-0">
                                        <Row>
                                            <Col className="col-7">
                                                <Card.Text className="subTotals p-0 my-3">
                                                    Shipping Subtotal:
                                                </Card.Text>
                                            </Col>
                                            <Col className="d-flex justify-content-end">
                                                <Card.Text className="subTotalPrices p-0 my-3">
                                                    ₱{shippingFeeSubtotal.toFixed(2)}
                                                </Card.Text>
                                            </Col>
                                        </Row>
                                    </Container>
                                    <Container fluid className="p-0">
                                        <Row>
                                            <Col className="col-7">
                                                <Card.Text className="subTotals p-0 my-3">
                                                    Items Subtotal:
                                                </Card.Text>
                                            </Col>
                                            <Col className="d-flex justify-content-end">
                                                <Card.Text className="subTotalPrices p-0 my-3">
                                                    ₱{checkOutContext.totalAmountToPay.toFixed(2)}
                                                </Card.Text>
                                            </Col>
                                        </Row>
                                    </Container>
                                    <Container fluid className="p-0">
                                        <Row>
                                            <Col className="col-7">
                                                <Card.Text className="subTotals p-0 my-3">
                                                    Total Payment:
                                                </Card.Text>
                                            </Col>
                                            <Col className="d-flex justify-content-end">
                                                <Card.Text className="subTotalPrices p-0 my-3">
                                                    ₱{totalAmountToPay.toFixed(2)}
                                                </Card.Text>
                                            </Col>
                                        </Row>
                                    </Container>

                                    <button
                                        className="placeOrderBtn"
                                        type="submit"
                                        onClick={() => {
                                            props.checkOutHandler();
                                        }}
                                    >
                                        Place Order
                                    </button>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </Modal.Body>
            </Modal>
        </Form>
    );
}
