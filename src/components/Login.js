import { useContext, useEffect, useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { userLogin } from "../api/UserApi";
import { CartContext } from "./store/CartContext";
import { UserContext } from "./store/UserContext";
import { Form, Button, Card, Container, Row, Col } from "react-bootstrap";
import "./Login.css";
import { OrderContext } from "./store/OrderContext";
import Swal from "sweetalert2";

export default function Login() {
    const userContext = useContext(UserContext);
    const cartContext = useContext(CartContext);
    const orderContext = useContext(OrderContext);
    const [emailInput, setEmailInput] = useState("");
    const [passwordInput, setPasswordInput] = useState("");
    const [isValidForm, setIsValidForm] = useState(false);

    useEffect(() => {
        if (emailInput && passwordInput) return setIsValidForm(true);
        setIsValidForm(false);
    }, [emailInput, passwordInput]);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const userCredentails = {
            email: emailInput,
            password: passwordInput,
        };

        const user = await userLogin(userCredentails);
        if (!user.token) {
            return Swal.fire({
                title: user,
                icon: "error",
                text: "Please Try Again",
            });
        }
        if (user.isAdmin) {
            localStorage.setItem("isAdmin", user.isAdmin);
        }

        localStorage.setItem("TestToken", user.token);
        userContext.setUserToken(localStorage.getItem("TestToken"));
        userContext.setUserAdmin(localStorage.getItem("isAdmin"));
        cartContext.loadUserCart();
        orderContext.onGetAll();
    };

    return userContext.token ? (
        userContext.isAdmin ? (
            <Navigate to="/adminPage" />
        ) : (
            <Navigate to="/shop" />
        )
    ) : (
        <Container fluid className="d-flex justify-content-center text-align-center mt-5">
            <Card className="loginCard">
                <Form onSubmit={handleSubmit}>
                    <Form.Label className="d-flex justify-content-center text-align-center m-4 loginHeader">
                        Login
                    </Form.Label>
                    <Row className="m-3">
                        <Col className="col-12">
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control
                                    type="email"
                                    placeholder="Enter email"
                                    onChange={(event) => {
                                        setEmailInput(event.target.value);
                                    }}
                                />
                            </Form.Group>
                        </Col>
                        <Col className="col-12">
                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="Password"
                                    onChange={(event) => {
                                        setPasswordInput(event.target.value);
                                    }}
                                />
                            </Form.Group>
                        </Col>
                        <Col className="col-12">
                            <Button
                                variant="primary"
                                type="submit"
                                className="w-100 loginBtn mt-2"
                                disabled={!isValidForm}
                            >
                                Login
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </Card>
        </Container>
    );
}
