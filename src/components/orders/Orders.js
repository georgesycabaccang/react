import { Fragment, useEffect, useState } from "react";
import { Card, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { orderRecieved } from "../../api/OrdersApi";

export default function Orders(props) {
    const [isReceived, setIsReceived] = useState(false);
    const orderDetails = props.orderDetails;
    const navigate = useNavigate();

    useEffect(() => {}, [isReceived]);

    let isPaid;
    if (orderDetails.isPaid === true) {
        isPaid = "Yes";
    } else {
        isPaid = "No";
    }

    let item;
    if (orderDetails.order.quantity > 1) {
        item = "items";
    } else {
        item = "item";
    }

    const isOrderPending = orderDetails.status === "Order Pending" && !isReceived;

    return (
        <Fragment>
            <Card>
                <Card.Body>
                    <Card.Title>{orderDetails.order.productName}</Card.Title>
                    <Card.Text>Order Status: {orderDetails.status}</Card.Text>
                    <Card.Text>
                        {orderDetails.order.quantity} {item}
                    </Card.Text>
                    <Card.Text>Payment Settled: {isPaid}</Card.Text>
                    <Card.Text>Order Total: ₱{orderDetails.totalAmoutToBePaid}</Card.Text>
                    {isOrderPending && (
                        <Button
                            type="submit"
                            variant="primary"
                            onClick={() => {
                                orderRecieved(orderDetails._id);
                                setIsReceived(true);
                                Swal.fire({
                                    title: "Good To Know",
                                    icon: "success",
                                    text: "Order Received!",
                                });
                            }}
                        >
                            Received
                        </Button>
                    )}
                </Card.Body>
            </Card>
        </Fragment>
    );
}
