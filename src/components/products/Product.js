import { Fragment, useContext, useState } from "react";
import { Link } from "react-router-dom";
import { Card, Container, Row, Col, Button } from "react-bootstrap";
import "./Product.css";
import { UserContext } from "../store/UserContext";
import { archiveOrUnarchiveProduct } from "../../api/ProductsApi";

export default function Product(props) {
    const userContext = useContext(UserContext);
    const [isArchived, setIsArchived] = useState(props.isAvailable);

    const archiveHandler = async () => {
        await archiveOrUnarchiveProduct(props.productId);
        setIsArchived(!isArchived);
    };
    return (
        <Fragment>
            <Link
                to={
                    !userContext.token
                        ? "/login"
                        : userContext.isAdmin
                        ? `/product/${props.productName}/${props.productId}/update`
                        : `/product/${props.productName}/${props.productId}`
                }
                className="text-decoration-none"
            >
                <Card className="m-2 itemHovered">
                    <Card.Img
                        variant="top"
                        src={`https://res.cloudinary.com/dpxtfxw52/image/upload/w_1000,h_1000,c_fill,q_100/${props.productImages[0]}`}
                        className=""
                    />
                    {props.discount !== "0%" && (
                        <Card.Text className="discount">{props.discount} Off</Card.Text>
                    )}
                    <Card.Body className="pb-2 px-0">
                        <Card.Title className="productName mb-3 px-2">
                            {props.productName}
                        </Card.Title>
                        <Container className="px-2">
                            <Row className="d-flex align-items-center">
                                <Col>
                                    <Card.Text className="price">₱{props.totalPrice}</Card.Text>
                                </Col>
                                <Col className="text-end">
                                    <Card.Text className="">
                                        <Card.Text className="totalSold">
                                            {props.totalBuyCount}
                                        </Card.Text>
                                        <Card.Text className="totalSoldText"> Sold</Card.Text>
                                    </Card.Text>
                                </Col>
                            </Row>
                        </Container>
                    </Card.Body>
                </Card>
            </Link>
            {userContext.isAdmin && (
                <Button
                    className="archiveBtn"
                    onClick={() => {
                        archiveHandler();
                    }}
                >
                    {!isArchived ? "Unarchive" : "Archive"}
                </Button>
            )}
        </Fragment>
    );
}
