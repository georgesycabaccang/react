import { Fragment, useContext, useEffect, useState } from "react";
import { PostProductContext } from "../store/PostProductContext";

export default function AddVariations(props) {
    const postProductContext = useContext(PostProductContext);
    const [variationTypeInput, setVariationTypeInput] = useState("");
    const [variationInput, setVariationInput] = useState("");
    const [isValid, setIsValid] = useState(false);

    // const obj = { hehe: "hoho" };
    // const keys = Object.keys(obj);
    // const values = Object.values(obj);
    // console.log(values);

    useEffect(() => {
        if (variationInput && variationTypeInput) return setIsValid(true);
        setIsValid(false);
    }, [variationInput, variationTypeInput]);

    const addVartionHandler = async (event) => {
        event.preventDefault();

        const arrayOfVariations = variationInput.split(", ");
        const variationDetails = {
            [variationTypeInput]: arrayOfVariations,
        };

        postProductContext.addVariation(variationDetails);
    };

    return (
        <Fragment>
            <form onSubmit={addVartionHandler}>
                <span>Variation Type:</span>
                <br />
                <input
                    type="text"
                    onChange={(event) => {
                        setVariationTypeInput(event.target.value);
                    }}
                />
                <br />
                <span>Variation:</span>
                <br />
                <h6>Please Enter Variations separated by a comma and a space &#40;," "&#41; </h6>
                <br />
                <input
                    type="text"
                    onChange={(event) => {
                        setVariationInput(event.target.value);
                    }}
                />
                <br />
                <button type="submit" disabled={!isValid}>
                    Add Variation
                </button>
                <br />
                <button
                    type="button"
                    onClick={() => {
                        props.setIsAddingVariation(false);
                    }}
                >
                    Cancel
                </button>
            </form>
        </Fragment>
    );
}
