import Product from "./Product";
import { Container, Row, Col } from "react-bootstrap";
import "./ProductList.css";

export default function ProductList(props) {
    return (
        <Container>
            <Row className="">
                {props.products.map((product) => {
                    return (
                        <Col xs={12} md={6} lg={2} className="p-0" key={product._id}>
                            <Product
                                key={product._id}
                                productId={product._id}
                                productImages={product.productImages}
                                productName={product.productName}
                                originalPrice={product.originalPrice}
                                totalPrice={product.totalPrice}
                                discount={product.discount}
                                description={product.description}
                                stock={product.stock}
                                totalBuyCount={product.totalBuyCount}
                                isAvailable={product.isAvailable}
                                productType={product.productType}
                            />
                        </Col>
                    );
                })}
            </Row>
        </Container>
    );
}
