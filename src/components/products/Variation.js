import VariationSpecifics from "./VariationSpecifics";
import "./Variation.css";

import { Card, ListGroup } from "react-bootstrap";
import { useContext } from "react";
import { PostProductContext } from "../store/PostProductContext";

export default function Variation(props) {
    const postProductContext = useContext(PostProductContext);
    const { id, ...details } = props.variationDetails;
    const variationSpecifics = Object.values(details);
    return (
        <ListGroup horizontal>
            <Card className="d-flex justify-content-center border-0 variationTypeContaier">
                <ListGroup.Item className="w-25 d-flex align-items-center border-0 p-0 variationTypeContaier">
                    <div className="toCapital removeButtonInline variationTypeTextSize">
                        {Object.keys(details)}
                    </div>
                    {!props.fromProductPage && (
                        <button
                            type="button"
                            className="removeButtonInline"
                            onClick={() => {
                                postProductContext.removeVariation(id);
                            }}
                        >
                            -
                        </button>
                    )}
                </ListGroup.Item>
            </Card>
            <ListGroup.Item className="border-0 p-0">
                <ListGroup horizontal>
                    {variationSpecifics[0].map((detail) => {
                        return <VariationSpecifics key={detail} detail={detail} id={id} />;
                    })}
                </ListGroup>
            </ListGroup.Item>
        </ListGroup>
    );
}
