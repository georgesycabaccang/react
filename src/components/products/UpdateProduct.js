import { useContext, useEffect, useState } from "react";
import { Form, Button, Card, Container, Row, Col, CloseButton } from "react-bootstrap";
import { Navigate, useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { getSingleProduct, updateProduct } from "../../api/ProductsApi";
import "./UpdateProduct.css";

export default function UpdateProduct(props) {
    const [loadedProduct, setLoadedProduct] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const { productId } = useParams();

    const [productName, setProductName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    const [discount, setDiscount] = useState("");
    const [stock, setStock] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        getProductDetails();
    }, []);

    const getProductDetails = async () => {
        const productDetails = await getSingleProduct(productId);
        setProductName(productDetails.productName);
        setDescription(productDetails.description);
        setPrice(productDetails.originalPrice);
        setDiscount(productDetails.discount);
        setStock(productDetails.stock);
        setLoadedProduct(productDetails);
        setIsLoading(false);
    };

    const updateHanlder = async (event) => {
        event.preventDefault();

        const productUpdates = {
            productName: productName,
            description: description,
            originalPrice: price,
            discount: discount,
            stock: stock,
        };

        const status = await updateProduct(productUpdates, productId);
        console.log(status);
        Swal.fire({
            title: `Product with ${productId} id has been updated.`,
            icon: "success",
        }).then(setTimeout(() => navigate("/shop"), 50));
    };

    return isLoading ? (
        <h2>Loading...</h2>
    ) : (
        <Container className="d-flex text-align-center justify-content-center">
            <Card className="updateProductCard p-4 mt-5 w-75">
                <CloseButton
                    className="mb-2"
                    onClick={() => {
                        navigate("/shop");
                    }}
                />
                <Form onSubmit={updateHanlder}>
                    <Row className="w-100">
                        <Col>
                            <Form.Group className="mb-3" controlId="productName">
                                <Form.Label>Product Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    value={productName}
                                    onChange={(event) => {
                                        setProductName(event.target.value);
                                    }}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="description">
                                <Form.Label>Description</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    row={5}
                                    value={description}
                                    onChange={(event) => {
                                        setDescription(event.target.value);
                                    }}
                                    className="descriptionTextArea"
                                />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-3" controlId="price">
                                <Form.Label>Price</Form.Label>
                                <Form.Control
                                    type="number"
                                    value={price}
                                    onChange={(event) => {
                                        setPrice(event.target.value);
                                    }}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="discount">
                                <Form.Label>Discount</Form.Label>
                                <Form.Control
                                    type="text"
                                    value={discount}
                                    onChange={(event) => {
                                        setDiscount(event.target.value);
                                    }}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="stock">
                                <Form.Label>Stock</Form.Label>
                                <Form.Control
                                    type="number"
                                    value={stock}
                                    onChange={(event) => {
                                        setStock(event.target.value);
                                    }}
                                />
                            </Form.Group>
                            <Button type="submit" className="updateProductBtn" variant="primary">
                                Update Product
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </Card>
        </Container>
    );
    // TRY TO ADD IMAGES AND VARIATIONS@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
}
