import { Col, Row } from "react-bootstrap";
import Variation from "./Variation";

export default function ProductVariationList(props) {
    return (
        <Row>
            <Col className="col-12">
                {props.productVariations.map((variation) => {
                    return (
                        <Variation
                            key={variation.id}
                            id={variation.id}
                            variationDetails={variation}
                            fromProductPage={true}
                        />
                    );
                })}
            </Col>
        </Row>
    );
}
