import { useContext, useState } from "react";
import { ListGroup } from "react-bootstrap";

import { VariationContext } from "../store/VariationContext";
import "./VariationSpecifics.css";

export default function VariationSpecifics(props) {
    const variationContext = useContext(VariationContext);

    return (
        <div className="variationList ">
            <ListGroup.Item className="border-0 m-0" id="list-item">
                <input
                    id={props.detail}
                    type="radio"
                    variant="light"
                    className="variationButton"
                    name={props.id}
                    onClick={() => {
                        variationContext.addVariation({
                            variationType: props.id,
                            variation: props.detail,
                        });
                    }}
                />
                <label className="toCapital">{props.detail}</label>
            </ListGroup.Item>
        </div>
    );
}
