import { Fragment, useContext } from "react";
import { Card } from "react-bootstrap";
import { PostProductContext } from "../store/PostProductContext";
import Variation from "./Variation";
import "./VariationList.css";

export default function VariationList() {
    const postProductContext = useContext(PostProductContext);
    return (
        <Fragment>
            <Card className="ps-0 ms-0 border-0">
                {postProductContext.productVariations.map((variation) => {
                    return (
                        <Variation
                            key={variation.id}
                            id={variation.id}
                            variationDetails={variation}
                            removeVariation={postProductContext.removeVariation.bind(
                                null,
                                variation
                            )}
                        />
                    );
                })}
            </Card>
        </Fragment>
    );
}

// FOR THE FUTURE!!!!!!!!!!!!!!!!!!!!! ADD EDIT FEATURE TO VARIATIONS
